package ru.kost.msg.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kost.msg.DTO.UserDto;

import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
public class Letter implements Serializable {
    private UUID uuid;
    private UserDto userDto;
    private boolean accept;

    @JsonCreator
    public Letter(@JsonProperty("uuid") UUID uuid
            , @JsonProperty("user") UserDto userDto
            , @JsonProperty("accept") boolean accept) {
        this.uuid = uuid;
        this.userDto = userDto;
        this.accept = accept;
    }
}

