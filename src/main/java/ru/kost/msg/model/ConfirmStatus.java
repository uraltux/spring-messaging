package ru.kost.msg.model;

public enum ConfirmStatus {
    CONFIRMED, DENIED
}
