package ru.kost.msg.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kost.msg.DTO.UserDto;
import ru.kost.msg.model.User;
import ru.kost.msg.service.Impl.MessageSenderImpl;
import ru.kost.msg.service.UserService;

@RestController
@Slf4j
public class UserRestController {
    private final UserService userService;
    private final MessageSenderImpl messageSender;

    @Autowired
    public UserRestController(UserService userService, MessageSenderImpl messageSender) {
        this.userService = userService;
        this.messageSender = messageSender;
    }


    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<String> add(@Validated UserDto userDto) {
        log.info("добавляю пользователя в базу");
        User user = userService.convertDto(userDto);
        userService.saveUser(user);
        log.info("отправляю сообщение");
        messageSender.sendMessage(userDto);
        return ResponseEntity.ok("put new user");
    }
}
