package ru.kost.msg.service;

import ru.kost.msg.DTO.UserDto;
import ru.kost.msg.model.User;

public interface UserService {
    void saveUser(User user);

    User getUserByEmail(String email);

    User convertDto(UserDto userDto);

}
