package ru.kost.msg.service.Impl;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.kost.msg.model.User;
import ru.kost.msg.service.EmailSender;
import ru.kost.msg.service.UserService;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service
@Slf4j
public class EmailSenderImpl implements EmailSender {
    private final UserService userService;

    public EmailSenderImpl(UserService userService) {
        this.userService = userService;
    }

    public void sendMail(String email, String messageBody) throws TimeoutException {
        if (shouldThrowTimeout()) {
            sleep();
            throw new TimeoutException("Timeout!");
        }
        if (shouldSleep()) {
            sleep();
        }
        log.info("Message sent to {}, body {}.", email, messageBody);
        User user = userService.getUserByEmail(email);
        user.setMailSend(true);
        userService.saveUser(user);
    }

    @SneakyThrows
    private static void sleep() {
        Thread.sleep(TimeUnit.MINUTES.toMillis(1));
    }

    private static boolean shouldSleep() {
        return new Random().nextInt(10) == 1;
    }

    private static boolean shouldThrowTimeout() {
        return new Random().nextInt(10) == 1;
    }
}
