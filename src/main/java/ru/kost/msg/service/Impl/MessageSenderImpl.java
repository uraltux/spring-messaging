package ru.kost.msg.service.Impl;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import ru.kost.msg.DTO.UserDto;
import ru.kost.msg.config.MessageConfig;
import ru.kost.msg.model.ConfirmStatus;
import ru.kost.msg.model.Letter;
import ru.kost.msg.model.User;
import ru.kost.msg.service.MessageSender;
import ru.kost.msg.service.UserService;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service
@Slf4j
public class MessageSenderImpl implements MessageSender {
    private final RabbitTemplate rabbitTemplate;
    private final UserService userService;

    public MessageSenderImpl(RabbitTemplate rabbitTemplate, UserService userService) {
        this.rabbitTemplate = rabbitTemplate;
        this.userService = userService;
    }
//формирование и отправка сообщения в шину
    public void sendMessage(UserDto user) {
        Letter letter = new Letter(UUID.randomUUID(), user, false);
        rabbitTemplate.convertAndSend(MessageConfig.EXCHANGE, MessageConfig.ROUTE_KEY, letter);
        log.info("Сообщение в шине {}", letter.toString());
    }
//принимаем обработанное сообщение
    public void receive(Letter letter) throws TimeoutException {
        log.info("Принято сообщение из шины {}", letter.getUuid());
        if (shouldThrowTimeout()) {
            sleep();
            log.info("Таймаут");
            throw new TimeoutException();
        }
        if (shouldSleep()) {
            sleep();
        }
        User user = userService.getUserByEmail(letter.getUserDto().getEmail());
        user.setStatus(letter.isAccept() ? ConfirmStatus.CONFIRMED : ConfirmStatus.DENIED);
        log.info("Анкета пользователя проверена, {}", user.getStatus());
        userService.saveUser(user);
        rabbitTemplate.convertAndSend(MessageConfig.EXCHANGE, MessageConfig.MAIL_ROUTE_KEY, letter);
    }

    @SneakyThrows
    private static void sleep() {
        Thread.sleep(TimeUnit.MINUTES.toMillis(1));
    }

    private static boolean shouldSleep() {
        boolean b = new Random().nextInt(10) == 1;
        log.info("будет ли ожидание обработки {}", b);
        return b;
    }

    private static boolean shouldThrowTimeout() {
        boolean b = new Random().nextInt(10) == 1;
        log.info("Будет ли таймаут {}", b);
        return b;
    }

}
