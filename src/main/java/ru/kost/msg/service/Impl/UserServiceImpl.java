package ru.kost.msg.service.Impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kost.msg.DAO.UserRepo;
import ru.kost.msg.DTO.UserDto;
import ru.kost.msg.model.User;
import ru.kost.msg.service.UserService;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepo userRepo;

    @Autowired
    public UserServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public void saveUser(User user) {
        userRepo.save(user);
        log.info("успешно сохранен {}", user);
    }

    @Override
    public User getUserByEmail(String email) {
        log.info("start find user by email " + email);
        return userRepo.findUserByEmail(email);
    }

    @Override
    public User convertDto(UserDto userDto) {
        User user = new User();
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setLogin(userDto.getLogin());
        user.setPassword(userDto.getPassword());
        return user;
    }
}
