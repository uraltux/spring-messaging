package ru.kost.msg.service.Impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import ru.kost.msg.config.MessageConfig;
import ru.kost.msg.model.Letter;
import ru.kost.msg.service.EmailSender;
import ru.kost.msg.service.MessageListener;

import java.util.Random;
import java.util.concurrent.TimeoutException;

@Service
@Slf4j
public class MessageListenerImpl implements MessageListener {
    private final RabbitTemplate rabbitTemplate;
    private final MessageSenderImpl messageSender;
    private final EmailSender emailSender;

    public MessageListenerImpl(RabbitTemplate rabbitTemplate, MessageSenderImpl messageSender, EmailSender emailSender) {
        this.rabbitTemplate = rabbitTemplate;
        this.messageSender = messageSender;
        this.emailSender = emailSender;
    }
//обработка сообщений из шины
    @RabbitListener(queues = MessageConfig.QUEUE)
    public void handleMessage(Letter letter) {
        log.info("Сообщение поступило на обработку {}", letter.getUuid());
        letter.setAccept(new Random().nextBoolean());
        log.info("установлен статус обработки сообщения {}", letter.isAccept());
        try {
            log.info("сообщение отправляется на сервер");
            messageSender.receive(letter);
        } catch (TimeoutException e) {
            log.info("сообщение не обработано, отправка обратно в очередь");
            rabbitTemplate.convertAndSend(MessageConfig.EXCHANGE, MessageConfig.ROUTE_KEY, letter);
        }
    }
//обработка сообщений поступающих на отправку email
    @RabbitListener(queues = MessageConfig.MAIL_QUEUE)
    public void handleMails(Letter letter) {
        log.info("получено сообщение в очередь на отправку {}", letter.getUuid());
        try {
            emailSender.sendMail(letter.getUserDto().getEmail(), letter.isAccept() ? "Ваша анкета подтверждена" : "Ваша анкета отклонена");
        } catch (TimeoutException e) {
            log.info("истекло время ожидания отправки email, возврат в очередь");
            rabbitTemplate.convertAndSend(MessageConfig.EXCHANGE, MessageConfig.MAIL_ROUTE_KEY, letter);

        }
    }
}
