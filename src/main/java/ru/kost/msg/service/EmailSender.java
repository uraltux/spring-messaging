package ru.kost.msg.service;

import java.util.concurrent.TimeoutException;

public interface EmailSender {
    void sendMail(String email, String messageBody) throws TimeoutException;
}
