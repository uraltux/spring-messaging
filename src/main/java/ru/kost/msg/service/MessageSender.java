package ru.kost.msg.service;

import ru.kost.msg.DTO.UserDto;
import ru.kost.msg.model.Letter;

import java.util.concurrent.TimeoutException;

public interface MessageSender {
    void sendMessage(UserDto user);

    void receive(Letter letter) throws TimeoutException;
}
