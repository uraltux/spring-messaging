package ru.kost.msg.DTO;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Null;
import java.io.Serializable;

@Data
public class UserDto implements Serializable {
    @Null
    private Long id;
    @NotBlank
    private String name;
    @Email
    private String email;
    @NotBlank
    private String login;
    @NotBlank
    private String password;

    public UserDto() {
    }

    @JsonCreator
    public UserDto(@JsonProperty("id") @Null Long id
            , @JsonProperty("name") @NotBlank String name
            , @JsonProperty("email") @Email String email
            , @JsonProperty("login") @NotBlank String login
            , @JsonProperty("password") @NotBlank String password) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.login = login;
        this.password = password;
    }
}
