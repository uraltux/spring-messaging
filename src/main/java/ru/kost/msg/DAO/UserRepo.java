package ru.kost.msg.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kost.msg.model.User;

public interface UserRepo extends JpaRepository<User, Long> {
    User findUserByEmail(String email);
}
